#School structure
""" 
Człowiek
Atrybuty: wiek, płeć, imię


Nauczyciel
Dziedziczy atrybuty z Klasy człowiek  i dodaje swoje: stanowisko, przedmiot, wynagrodzenie
Metody: zmiania stopni uczniów


Uczeń
Dziedziczy atrybuty z Klasy człowiek i dodaje swoje: numer klasy i grafik 
Metody: 
Szkolne rezultaty - określają stopnie i wyróżnienia
Zmiana klasy - pozwala na zmianę klasy ucznia


Numer_Klasy atrybuty: nazwa i maksymalna liczba uczniów
Metody - dodawanie ucznia do  klasy

 """
class Human:
    
    def __init__(self, name, age, gender,):
        self.name = name
        self.age = age
        self.gender = gender

    
class Teacher(Human):

    def __init__(self,name, age, gender, position, subject, salary):
        super().__init__(name, age, gender)
        self.position = position
        self.subject = subject
        self.salary= salary
    
    def student_exam(self, student, new_degrees):
        student.degrees = new_degrees



class Student(Human): 

    def __init__(self,name, age, gender, class_num, timetable):
         super().__init__(name, age, gender)
         self.class_num = class_num
         self.timetable = timetable

    def school_results(self, degrees, distintions):
        self.degrees = degrees
        self.distintions = distintions

    def change_class(self, class_num, new_class_num):
        self.class_num = new_class_num


class Class_Num: 

    def __init__(self, name, max_students):
        self.name = name
        self.max_students = max_students
        self.students = []

    def add_student(self, student):
        if len(self.students) < self.max_students:
            self.students.append(student)
        else: print("This class is full")
