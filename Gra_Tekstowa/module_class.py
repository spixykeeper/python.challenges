#module_with_Class

import random
import sys
import time

class Room():

    def __init__(self, name, money):
        self.name = name
        self.money = money

    def start(self):
        print("Witaj Nieznajomy ;)\nZnajdujesz się w Kasynie 'Szybka Kasiorka'. Skoro tu jesteś to potrzebujesz dużej i łatwej gotówki, chętnie pomożemy Ci ją zdobyć!")
        self.user_name = input("Jak się nazywasz?: ")
        ask_start = input("Gotowy do zabawy? Wpisz: 'tak' by rozpocząć podróż po Kasynie lub wpisz 'nie' by z niego wyjść. ")
        if ask_start.lower() == "tak":
            print(' ')
            room.menu()
        if ask_start.lower() == "nie":
            print(' ')
            room.exit()

    def menu(self):
        print(f"{self.user_name} znajdujesz się w MENU kasyna.\nMasz przed sobą kilka opcji. Wpisz:\n-'start' by powrócić do wyboru imienia,\n-'koniec' by opuscić kasyno,\n-'portfel by zobaczyc menu portfela',\n-'gra' by zagrac w grę")
        while True:
            ask_menu = input("Co chcesz zrobić?: ")
            if ask_menu.lower() == 'start':
                print(' ')
                room.start()
            elif ask_menu.lower() == 'koniec':
                print(' ')
                room.exit()
            elif ask_menu.lower() == 'portfel':
                print(' ')
                room.pocket()
            elif ask_menu.lower() == 'gra':
                print(' ')
                room.game1()
            else: print("Nie ma takiej komendy\n    ")

    def pocket(self):
        print(f"{self.user_name} to Twój wirtualny portfel. Musisz wpłacić do niego pieniądze, by rozpocząć grę.")
        print("Możesz pieniądze, wpłacic, wypłacić, lub zobaczyć stan portfela. Wpisz:\n-'wplacam' by wpłacić\n-'wyplacam' by wypłacić\n-'stan' by zobaczyć stan portfela\n-'powrot' by wrocic do MENU:  ")
        while True:
            print(' ')
            ask_pocket = input("Co chcesz zrobić? ")
            if ask_pocket.lower() == "wplacam":
                self.add_money = input("Ile pieniędzy chcesz wpłacić do portfela?: " )
                self.add_money = int(self.add_money)
                self.money = self.money + self.add_money
            elif ask_pocket.lower() == "stan":
                print(f'Stan Twojego portfela to: {self.money} $')
            elif ask_pocket.lower() == "wyplacam":
                ask_wyplata = input(f"Stan Twojego portfeta to {self.money} $, ile chcesz wypłacić? ")
                ask_wyplata = int(ask_wyplata)
                if ask_wyplata <= self.money:
                    self.money = self.money - ask_wyplata
                else:
                    print(f"Nie masz tyle pieniędzy. Stan Twojego portfela to {self.money} $\nZostaniesz przeniesiony do menu portfela")
                    time.sleep(3)
                    print(' ')
                    room.pocket()
            elif ask_pocket.lower() == "powrot":
                print("Zostaniesz przeniesiony do MENU")
                print(' ')
                time.sleep(2)
                room.menu()
            else: print("Nie ma takiej komendy")
        


    def exit(self):
        print("Czy jesteś pewny, że chcesz opuścić kasyno? Wpisz 'tak' by wyjść lub 'nie' by wrócić do MENU")
        answer_exit = input()
        if answer_exit.lower() == "tak":
            print(f"Przykro nam, że opuszczasz nasze kasyno. Aktualny stan Twojego portfela to: {self.money} $\nMamy nadzieję, że do nas wrócisz!")
            sys.exit()
            
        elif answer_exit.lower() == "nie":
            print("Słuszna decyzja! Gramy dalej.\nZostaniesz przeniesiony do MENU Gry")
            time.sleep(1.5)
            print(' ')
            room.menu()
            

    def game1(self):
        if self.money == 0:
            print("Stan Twojego portfela to 0 $. Wpłać pieniądze do portela by zagrać w grę.\nZostaniesz przeniesiony do MENU gry")
            time.sleep(2)
            print(' ')
            room.menu()
        print("Witaj w Grze 'Szybki Bill'\nZasady są proste. Z losowo dobieranych liter 'I N W'  odbędzie się losowanie. Wygrywasz w momencie, w którym litery ułożą się w słowo 'WIN'.")
        print("Wygrana to aż  5-krotność tego co obstawiłeś? Myślisz, że się nie uda? A co jeśli my za Ciebie automatycznie ustawimy pierwszą literę?\nZostały już tylko dwie. To jak, gotowy na grę? ;] ")

        while True:
            print(' ')
            user_ask = input(f"To jak {self.user_name}, gramy? Wystarczy, że wpiszesz 'tak' i zaczynamy rundę. Jeśli chcesz zrezygnować napisz 'nie': ")
            if user_ask.lower() == "tak":
                game1_input = input(f"Masz do dyspozycji następującą liczbę pieniędzy: {self.money} $. Ile chcesz obstawić? ")
                game1_input = int(game1_input)
                if game1_input <= self.money:
                    lottery = ["W", "I", "N"]
                    result_1 = []
                    for x in  lottery:
                        random.shuffle(lottery)
                        result_1.append(x)
                    print(result_1)
            
                    
                    if result_1 == ["W", "I", "N"] :
                        self.money = self.money + game1_input * 5
                        print(f"Wygrałeś!!\nStawka którą obstawiłeś czyli: {game1_input} $ została powiększona  5-krotnie.")
                        print(f'Twój obecny stan portfela to: {self.money} $')

                    elif result_1 != ["W", "I", "N"] :
                        print("OOPS...było blisko, spróbój ponownie!")
                        self.money = self.money - game1_input
                        if self.money == 0:
                            print(' ')
                            time.sleep(1.5)
                            print(f"{self.user_name} stan Twojego portfela to 0$.\nSkonczyły Ci się pieniądze. Musisz wpłacić pieniądze do swojego portfela.\nZostaniesz przeniesiony do MENU gry")
                            print(' ')
                            time.sleep(1.5)
                            room.menu()
                        print(f"Twój aktualny stan portfela to: {self.money} $")
                else: print("Nie masz masz tyle kasiorki")
            if user_ask.lower() == 'nie':
                print("Zostaniesz przeniesiony do MENU Kasyna")
                time.sleep(1.5)
                print(' ')
                room.menu()
                
room = Room("Szybka Kasiorka", 0)