from datetime import date
import time
import sys
#function to check user adult

def adult_check_func():
    print("Witaj!")
    time.sleep(1)
    print("To gra przeznaczona dla osób, które ukończyły 18 rok życia.")
    time.sleep(1)
    print("Podaj datę swojego urodzenia: ")
    time.sleep(1)
    birth_day = input("wpisz dzień: ")
    birth_month = input("wpisz miesiąc: " )
    birth_year =  input("Wpisz rok: ")
    birth_year = int(birth_year)
    birth_month = int(birth_month)
    birth_day = int(birth_day)

    def calculateAge(birthDate):
        today = date.today()
        age = today.year - birthDate.year - ((today.month, today.day) < (birthDate.month, birthDate.day))

        return age
    
    check = calculateAge(date(birth_year, birth_month, birth_day))
    if check >= 18:
        print(f'Masz {check} lat. Miłej gry!')
        time.sleep(1)
    elif check < 18:
        print(f"Masz {check} lat, nie możesz grać w tą grę")
        time.sleep(1)
        print("Zapraszamy jak ukończysz 18 lat!")
        sys.exit()


