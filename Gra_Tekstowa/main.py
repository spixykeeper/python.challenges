from module_class import Room, room
from adult_check import adult_check_func


def game():
    adult_check_func()
    room.start()
    

if __name__ == "__main__":
    game()