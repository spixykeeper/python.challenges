#Challenge 9.7
#Capital City Loop

import random

while True: #main game loop

    capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
    }

    losowo = list(capitals_dict.items()) 
    random.shuffle(losowo)   
    for state, capital in losowo:
        state = state
        capital = capital

    user_answer = input((f"Hello user, what is the capital of state {state}: "))
    if user_answer == capitals_dict[state]:
        print("Correct")
        break
    elif user_answer.lower() == "exit":
        print(f"The correct answer was: {capital} ")
        print("Goodbye!")
        break
    elif user_answer != capitals_dict[state]:
        print("Incorrect, try again")
        continue
    
