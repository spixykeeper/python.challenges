#Challenge 9.5
#Challenge :  Challenge: Wax Poetic

import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

def poem(): # funtion to generate random words from lists
    # generate random nouns
    while True:
        n1 = random.choice(nouns)
        n2 = random.choice(nouns)
        n3 = random.choice(nouns)
        if n1 != n2 and n2 != n3 and n3 != n1:
            break
              
    while True:
    # generate random verbs
        v1 = random.choice(verbs)
        v2 = random.choice(verbs)
        v3 = random.choice(verbs)
        if v1 != v2 and v2 != v3 and v3 != v1:
            break

    while True:
        # generate random adjectives
        adj1 = random.choice(adjectives)
        adj2 = random.choice(adjectives)
        adj3 = random.choice(adjectives)
        if adj1 != adj2 and adj2 != adj3 and adj3 != adj1:
            break

    while True:
        # generate random prepositions
        prep1 = random.choice(prepositions)
        prep2 = random.choice(prepositions)
        if prep1 != prep2:
            break

        #generate random adverb
    adv1 = random.choice(adverbs)

        #generate respective "A" or "An" 
    if "aeouy".find(adv1[0]) != -1:
        word = "A"
    else:
        word = "An"

            
            
    poemat = (
        f"{word} {adj1} {n1}\n\n"
        f"{word} {adj1} {n1} {v1} {prep1} the {adj2} {n2}\n"
        f"{adv1}, the {n1} {v2}\n"
        f"the {n2} {v3} {prep2} a {adj3} {n3}"
    )
    
    return poemat
        
        

if __name__ == '__main__':
    print(poem())
