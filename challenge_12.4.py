# 12.4 Challenge
# Move All Image Files To a New Directory

from pathlib import Path

documents_dir = (
    Path.home() /
    "python-basics-exercises" /
    "ch11-file-input-and-output" /
    "practice_files" /
    "documents"
)

images_dir = Path.home() / "images"
images_dir.mkdir(exist_ok=True)

for path in documents_dir.rglob("*.*"):
    if path.suffix.lower() in [".png", ".jpg", ".gif"]:
        path.replace(images_dir / path.name)